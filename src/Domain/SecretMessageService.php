<?php

namespace Webvogel\SecretMessage\Domain;

use Species\Common\Value\Uuid\Exception\InvalidUuid;
use Webvogel\SecretMessage\Domain\Exception\{InvalidPassword, MaxHitsExceeded, SecretMessageNotFound, TokenExpired};
use Webvogel\SecretMessage\Domain\Model\SecretMessage;
use Webvogel\SecretMessage\Domain\Model\Value\{EncryptedMessage, Password, Recipient, SecretMessageId, SecretMessageToken};
use Webvogel\SecretMessage\Domain\Storage\SecretMessageStorage;

/**
 * Secret message service.
 */
final class SecretMessageService
{

    /** @var SecretMessageStorage */
    private $storage;

    /** @var CryptoBox */
    private $cryptoBox;



    /**
     * @param SecretMessageStorage $storage
     * @param CryptoBox            $cryptoBox
     */
    public function __construct(SecretMessageStorage $storage, CryptoBox $cryptoBox)
    {
        $this->storage = $storage;
        $this->cryptoBox = $cryptoBox;
    }



    /**
     * @param string $id
     * @return SecretMessage
     */
    public function fetch(string $id): SecretMessage
    {
        return $this->storage->fetch(SecretMessageId::fromString($id));
    }

    /**
     * @param string $token
     * @return SecretMessage
     */
    public function fetchByToken(string $token): SecretMessage
    {
        return $this->storage->fetchByToken(SecretMessageToken::fromString($token));
    }

    /**
     * @return SecretMessage[]
     */
    public function fetchAll(): array
    {
        return $this->storage->fetchAll();
    }



    /**
     * @param string                  $plainMessage
     * @param string                  $recipient
     * @param string|null             $plainPassword
     * @param \DateTimeImmutable|null $expirationDate = null
     * @param int|null                $maxHits        = null
     * @return SecretMessage
     */
    public function create(
        string $plainMessage,
        string $recipient,
        ?string $plainPassword = null,
        ?\DateTimeImmutable $expirationDate = null,
        ?int $maxHits = null
    ): SecretMessage
    {
        $secretMessage = SecretMessage::create(
            $this->generateId(),
            $this->generateToken(),
            EncryptedMessage::fromString($this->cryptoBox->encrypt($plainMessage)),
            Recipient::fromString($recipient),
            !empty($plainPassword) ? Password::hash($plainPassword) : null,
            $expirationDate,
            $maxHits
        );

        $this->storage->save($secretMessage);

        return $secretMessage;
    }

    /**
     * @param SecretMessage $secretMessage
     */
    public function remove(SecretMessage $secretMessage): void
    {
        $this->storage->remove($secretMessage);
    }



    /**
     * @param string      $token
     * @param string|null $plainPassword = null
     * @return string
     * @throws InvalidPassword
     * @throws MaxHitsExceeded
     * @throws TokenExpired
     */
    public function open(string $token, ?string $plainPassword = null): string
    {
        $secretMessage = $this->storage->fetchByToken(SecretMessageToken::fromString($token));
        $secretMessage->hit($plainPassword);
        $this->storage->save($secretMessage);

        return $this->cryptoBox->decrypt($secretMessage->getMessage());
    }



    /**
     * @param string $id
     * @return bool
     */
    public function hasId(string $id): bool
    {
        try {
            return (bool)$this->storage->fetch(SecretMessageId::fromString($id));
        } catch (InvalidUuid|SecretMessageNotFound $e) {
            return false;
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    public function hasToken(string $id): bool
    {
        try {
            return (bool)$this->storage->fetchByToken(SecretMessageToken::fromString($id));
        } catch (SecretMessageNotFound $e) {
            return false;
        }
    }



    /**
     * @return SecretMessageId
     */
    public function generateId(): SecretMessageId
    {
        do {
            $id = SecretMessageId::generate();
        } while ($this->hasId($id));

        return $id;
    }

    /**
     * @return SecretMessageToken
     */
    public function generateToken(): SecretMessageToken
    {
        do {
            $token = SecretMessageToken::generate();
        } while ($this->hasToken($token));

        return $token;
    }

}
