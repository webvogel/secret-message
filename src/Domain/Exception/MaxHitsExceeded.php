<?php

namespace Webvogel\SecretMessage\Domain\Exception;

/**
 * Max hits exceeded exception.
 */
final class MaxHitsExceeded extends \Exception
{

}
