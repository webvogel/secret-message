<?php

namespace Webvogel\SecretMessage\Domain\Exception;

/**
 * Secret message not found exception.
 */
final class SecretMessageNotFound extends \DomainException
{

}
