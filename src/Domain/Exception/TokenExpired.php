<?php

namespace Webvogel\SecretMessage\Domain\Exception;

/**
 * Token expired exception.
 */
final class TokenExpired extends \Exception
{

}
