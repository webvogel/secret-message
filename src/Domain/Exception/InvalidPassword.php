<?php

namespace Webvogel\SecretMessage\Domain\Exception;

/**
 * Invalid password exception.
 */
final class InvalidPassword extends \Exception
{

}
