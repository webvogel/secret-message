<?php

namespace Webvogel\SecretMessage\Domain\Exception;

/**
 * Duplicate secret message ID exception.
 */
final class DuplicateSecretMessageId extends \InvalidArgumentException
{

}
