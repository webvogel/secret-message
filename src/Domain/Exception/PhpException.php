<?php

namespace Webvogel\SecretMessage\Domain\Exception;

/**
 * Php exception.
 */
final class PhpException extends \RuntimeException
{

    /**
     * @param \Exception $e
     * @return PhpException
     */
    public static function fromException(\Exception $e): self
    {
        return new self($e->getMessage(), $e->getCode(), $e->getPrevious());
    }

}
