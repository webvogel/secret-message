<?php

namespace Webvogel\SecretMessage\Domain\Exception;

/**
 * Duplicate secret message token exception.
 */
final class DuplicateSecretMessageToken extends \InvalidArgumentException
{

}
