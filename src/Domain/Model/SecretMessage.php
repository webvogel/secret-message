<?php

namespace Webvogel\SecretMessage\Domain\Model;

use Webvogel\SecretMessage\Domain\Exception\InvalidPassword;
use Webvogel\SecretMessage\Domain\Exception\MaxHitsExceeded;
use Webvogel\SecretMessage\Domain\Exception\PhpException;
use Webvogel\SecretMessage\Domain\Exception\TokenExpired;
use Webvogel\SecretMessage\Domain\Model\Value\EncryptedMessage;
use Webvogel\SecretMessage\Domain\Model\Value\Hit;
use Webvogel\SecretMessage\Domain\Model\Value\Hits;
use Webvogel\SecretMessage\Domain\Model\Value\Password;
use Webvogel\SecretMessage\Domain\Model\Value\Recipient;
use Webvogel\SecretMessage\Domain\Model\Value\SecretMessageId;
use Webvogel\SecretMessage\Domain\Model\Value\SecretMessageToken;

/**
 * Secret message entity.
 */
final class SecretMessage
{

    /** @var SecretMessageId */
    private $id;

    /** @var SecretMessageToken */
    private $token;

    /** @var EncryptedMessage */
    private $message;

    /** @var Recipient */
    private $recipient;

    /** @var \DateTimeImmutable */
    private $createDate;

    /** @var Password|null */
    private $password;

    /** @var \DateTimeImmutable|null */
    private $expirationDate;

    /** @var int|null */
    private $maxHits;

    /** @var Hits */
    private $hits;



    /**
     * @param SecretMessageId         $id
     * @param SecretMessageToken      $token
     * @param EncryptedMessage        $message
     * @param Recipient               $recipient
     * @param Password|null           $password
     * @param \DateTimeImmutable|null $expirationDate
     * @param int|null                $maxHits
     * @return SecretMessage
     */
    public static function create(
        SecretMessageId $id,
        SecretMessageToken $token,
        EncryptedMessage $message,
        Recipient $recipient,
        ?Password $password,
        ?\DateTimeImmutable $expirationDate,
        ?int $maxHits
    )
    {
        $createDate = self::now();
        $hits = new Hits();

        return new self($id, $token, $message, $recipient, $createDate, $password, $expirationDate, $maxHits, $hits);
    }



    /**
     * @param SecretMessageId         $id
     * @param SecretMessageToken      $token
     * @param EncryptedMessage        $message
     * @param Recipient               $recipient
     * @param \DateTimeImmutable      $createDate
     * @param Password|null           $password
     * @param \DateTimeImmutable|null $expirationDate
     * @param int|null                $maxHits
     * @param Hits                    $hits
     */
    public function __construct(
        SecretMessageId $id,
        SecretMessageToken $token,
        EncryptedMessage $message,
        Recipient $recipient,
        \DateTimeImmutable $createDate,
        ?Password $password,
        ?\DateTimeImmutable $expirationDate,
        ?int $maxHits,
        Hits $hits
    )
    {
        if ($maxHits !== null && $maxHits < 1) {
            throw new \InvalidArgumentException();
        }

        $this->id = $id;
        $this->token = $token;
        $this->message = $message;
        $this->recipient = $recipient;
        $this->createDate = $createDate;
        $this->password = $password;
        $this->maxHits = $maxHits;
        $this->expirationDate = $expirationDate;
        $this->hits = $hits;
    }



    /** @return SecretMessageId */
    public function getId(): SecretMessageId
    {
        return $this->id;
    }

    /** @return SecretMessageToken */
    public function getToken(): SecretMessageToken
    {
        return $this->token;
    }

    /** @return EncryptedMessage */
    public function getMessage(): EncryptedMessage
    {
        return $this->message;
    }

    /** @return Recipient */
    public function getRecipient(): Recipient
    {
        return $this->recipient;
    }

    /** @return \DateTimeImmutable */
    public function getCreateDate(): \DateTimeImmutable
    {
        return $this->createDate;
    }

    /** @return Password|null */
    public function getPassword(): ?Password
    {
        return $this->password;
    }

    /** @return \DateTimeImmutable|null */
    public function getExpirationDate(): ?\DateTimeImmutable
    {
        return $this->expirationDate;
    }

    /** @return int|null */
    public function getMaxHits(): ?int
    {
        return $this->maxHits;
    }

    /** @return Hits */
    public function getHits(): Hits
    {
        return $this->hits;
    }



    /**
     * @return int|null
     */
    public function hitsLeft(): ?int
    {
        if ($this->maxHits === null) {
            return null;
        }

        return max(0, $this->maxHits - $this->hits->count());
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        if ($this->expirationDate === null) {
            return false;
        }

        return $this->expirationDate < self::now();
    }

    /**
     * @return bool
     */
    public function isMaxHitsExceeded(): bool
    {
        if ($this->maxHits === null) {
            return false;
        }

        return $this->maxHits <= $this->hits->count();
    }



    /**
     * @param string|null $plainPassword
     * @throws InvalidPassword
     * @throws MaxHitsExceeded
     * @throws TokenExpired
     */
    public function hit(?string $plainPassword = null): void
    {
        if ($this->isExpired()) {
            throw new TokenExpired();
        }
        if ($this->isMaxHitsExceeded()) {
            throw new MaxHitsExceeded();
        }
        if ($this->password !== null && !$this->password->verify("$plainPassword")) {
            throw new InvalidPassword();
        }

        $this->hits = $this->hits->add(Hit::generate());
    }



    /**
     * @return \DateTimeImmutable
     */
    private static function now(): \DateTimeImmutable
    {
        try {
            return new \DateTimeImmutable();
        } catch (\Exception $e) {
            throw PhpException::fromException($e);
        }
    }

}
