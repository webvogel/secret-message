<?php

namespace Webvogel\SecretMessage\Domain\Model\Value;

use Species\Common\Value\String\LabelValue;

/**
 * Recipient value object.
 */
final class Recipient extends LabelValue
{

}
