<?php

namespace Webvogel\SecretMessage\Domain\Model\Value;

use Species\Common\Value\Uuid\Uuid4Value;

/**
 * Secret message ID value object.
 */
final class SecretMessageId extends Uuid4Value
{

}
