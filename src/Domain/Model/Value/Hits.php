<?php

namespace Webvogel\SecretMessage\Domain\Model\Value;

/**
 * Immutable hit collection (numeric keys).
 */
final class Hits implements \IteratorAggregate, \Countable
{

    /** @var Hit[] */
    private $hits = [];



    /**
     * @param iterable|Hit[] $hits = []
     */
    public function __construct(iterable $hits = [])
    {
        // hits must be unique
        foreach ($hits as $hit) {
            if (!in_array($hit, $this->hits, true)) {
                $this->hits[] = $hit;
            }
        }

        // values must be of type Hit and sorted by date
        usort($this->hits, function (Hit $a, Hit $b) {
            return $a->getDate() <=> $b->getDate();
        });
    }



    /**
     * @inheritdoc
     * @yield Hit
     * @return \Generator|Hit[]
     */
    public function getIterator(): \Generator
    {
        yield from $this->hits;
    }

    /** @inheritdoc */
    public function count(): int
    {
        return count($this->hits);
    }



    /**
     * @return Hit|null
     */
    public function first(): ?Hit
    {
        return $this->hits ? $this->hits[0] : null;
    }

    /**
     * @return Hit|null
     */
    public function last(): ?Hit
    {
        $count = count($this->hits);

        return $count ? $this->hits[$count - 1] : null;
    }



    /**
     * @param Hit $hit
     * @return self
     */
    public function add(Hit $hit): self
    {
        $hits = $this->hits;
        $hits[] = $hit;

        return new self($hits);
    }

}
