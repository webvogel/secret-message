<?php

namespace Webvogel\SecretMessage\Domain\Model\Value;

use Species\Common\Value\String\StringValue;

/**
 * Encrypted message value object.
 */
final class EncryptedMessage extends StringValue
{

}
