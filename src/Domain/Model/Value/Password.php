<?php

namespace Webvogel\SecretMessage\Domain\Model\Value;

use Species\Common\Value\Password\BcryptPasswordValue;
use Species\Common\Value\Password\Exception\InvalidPlainPassword;

/**
 * Password value object.
 */
final class Password extends BcryptPasswordValue
{

    /** @inheritdoc */
    protected static function guardPlainPassword(string $plainPassword): void
    {
        // must be at least 6 long
        if (strlen($plainPassword) < 6) {
            throw new InvalidPlainPassword();
        }

        // no whitespaces allowed
        if (preg_match('/\s/S', $plainPassword)) {
            throw new InvalidPlainPassword();
        }
    }

}
