<?php

namespace Webvogel\SecretMessage\Domain\Model\Value;

use Webvogel\SecretMessage\Domain\Exception\PhpException;

/**
 * Hit value object.
 */
final class Hit
{

    /** @var \DateTimeImmutable */
    private $date;



    /**
     * @return self
     */
    public static function generate(): self
    {
        try {
            return new self(new \DateTimeImmutable());
        } catch (\Exception $e) {
            throw PhpException::fromException($e);
        }
    }



    /**
     * @param \DateTimeImmutable $date
     */
    public function __construct(\DateTimeImmutable $date)
    {
        $this->date = $date;
    }



    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

}
