<?php

namespace Webvogel\SecretMessage\Domain\Model\Value;

use Species\Common\Value\String\StringValue;
use Webvogel\SecretMessage\Domain\Exception\PhpException;

/**
 * Secret message token value object.
 */
final class SecretMessageToken extends StringValue
{

    const LENGTH = 64;
    const KEY_SPACE = '-_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';



    /**
     * @return self
     */
    public static function generate()
    {
        $maxIndex = strlen(self::KEY_SPACE) - 1;

        try {
            $token = '';
            for ($i = 0; $i < self::LENGTH; ++$i) {
                $token .= self::KEY_SPACE[random_int(0, $maxIndex)];
            }
        } catch (\Exception $e) {
            throw PhpException::fromException($e);
        }

        return self::fromString($token);
    }

}
