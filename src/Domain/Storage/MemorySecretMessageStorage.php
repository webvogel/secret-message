<?php

namespace Webvogel\SecretMessage\Domain\Storage;

use Webvogel\SecretMessage\Domain\Exception\DuplicateSecretMessageId;
use Webvogel\SecretMessage\Domain\Exception\DuplicateSecretMessageToken;
use Webvogel\SecretMessage\Domain\Exception\SecretMessageNotFound;
use Webvogel\SecretMessage\Domain\Model\SecretMessage;
use Webvogel\SecretMessage\Domain\Model\Value\SecretMessageId;
use Webvogel\SecretMessage\Domain\Model\Value\SecretMessageToken;

/**
 * Memory secret message storage.
 */
final class MemorySecretMessageStorage implements SecretMessageStorage
{

    /** @var SecretMessage[] */
    private $secretMessages = [];



    /** @inheritdoc */
    public function fetch(SecretMessageId $id): SecretMessage
    {
        $key = $id->toString();
        if (!isset($this->secretMessages[$key])) {
            throw new SecretMessageNotFound();
        }

        return $this->secretMessages[$key];
    }

    /** @inheritdoc */
    public function fetchByToken(SecretMessageToken $token): SecretMessage
    {
        foreach ($this->secretMessages as $secretMessage) {
            if ($secretMessage->getToken()->sameAs($token)) {
                return $secretMessage;
            }
        }

        throw new SecretMessageNotFound();
    }

    /** @inheritdoc */
    public function fetchAll(): array
    {
        $secretMessages = $this->secretMessages;
        usort($secretMessages, function (SecretMessage $a, SecretMessage $b) {
            return $b->getCreateDate() <=> $a->getCreateDate(); // sort newest first
        });

        return $secretMessages;
    }



    /** @inheritdoc */
    public function save(SecretMessage $secretMessage): void
    {
        try {
            if ($secretMessage === $this->fetch($secretMessage->getId())) {
                return; // already stored
            }
            throw new DuplicateSecretMessageId();
        } catch (SecretMessageNotFound $e) {
        }

        try {
            $this->fetchByToken($secretMessage->getToken());
            throw new DuplicateSecretMessageToken();
        } catch (SecretMessageNotFound $e) {
        }

        $key = $secretMessage->getId()->toString();
        $this->secretMessages[$key] = $secretMessage;
    }

    /** @inheritdoc */
    public function remove(SecretMessage $secretMessage): void
    {
        $this->fetch($secretMessage->getId()); // test existence
        $this->save($secretMessage); // test duplicate entity

        $key = $secretMessage->getId()->toString();
        unset($this->secretMessages[$key]);
    }

}
