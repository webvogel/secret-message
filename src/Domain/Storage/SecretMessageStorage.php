<?php

namespace Webvogel\SecretMessage\Domain\Storage;

use Webvogel\SecretMessage\Domain\Exception\{DuplicateSecretMessageId, DuplicateSecretMessageToken, SecretMessageNotFound};
use Webvogel\SecretMessage\Domain\Model\SecretMessage;
use Webvogel\SecretMessage\Domain\Model\Value\{SecretMessageId, SecretMessageToken};

/**
 * Secret message storage interface.
 */
interface SecretMessageStorage
{

    /**
     * @param SecretMessageId $id
     * @return SecretMessage
     * @throws SecretMessageNotFound
     */
    public function fetch(SecretMessageId $id): SecretMessage;

    /**
     * @param SecretMessageToken $token
     * @return SecretMessage
     * @throws SecretMessageNotFound
     */
    public function fetchByToken(SecretMessageToken $token): SecretMessage;

    /**
     * @return SecretMessage[]
     */
    public function fetchAll(): array;



    /**
     * @param SecretMessage $secretMessage
     * @throws DuplicateSecretMessageId
     * @throws DuplicateSecretMessageToken
     */
    public function save(SecretMessage $secretMessage): void;

    /**
     * @param SecretMessage $secretMessage
     * @throws DuplicateSecretMessageId
     * @throws SecretMessageNotFound
     */
    public function remove(SecretMessage $secretMessage): void;

}
