<?php

namespace Webvogel\SecretMessage\Domain;

use Webvogel\SecretMessage\Domain\Exception\PhpException;

/**
 * Crypto Box implementation of libsodium NaCl (Xsals20 + Poly1305).
 */
final class CryptoBox
{

    /** @var string */
    private $key;



    /**
     * @return string
     */
    public static function generateKey(): string
    {
        try {
            return random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES);
        } catch (\Exception $e) {
            throw PhpException::fromException($e);
        }
    }



    /**
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }



    /**
     * Hide key for var_dump, print_r, ...
     *
     * @inheritdoc
     */
    public function __debugInfo(): array
    {
        return ['key' => '***'];
    }

    /**
     * Don't allow serialization.
     *
     * @inheritdoc
     * @throws \Exception
     */
    public function serialize(): string
    {
        throw new \Exception(self::class . ' cannot be serialized.');
    }



    /**
     * @param string $string
     * @return string
     */
    public function encrypt(string $string): string
    {
        try {
            $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        } catch (\Exception $e) {
            throw PhpException::fromException($e);
        }
        $cipherText = sodium_crypto_secretbox($string, $nonce, $this->key);
        $encoded = base64_encode($nonce . $cipherText);

        return $encoded;
    }

    /**
     * @param string $string
     * @return string
     */
    public function decrypt(string $string): string
    {
        $decoded = base64_decode($string);
        $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $cipherText = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');
        $decoded = sodium_crypto_secretbox_open($cipherText, $nonce, $this->key);

        return $decoded;
    }

}
